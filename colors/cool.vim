"%% SiSU Vim color file
" Slate Maintainer: Ralph Amissah <ralph@amissah.com>
" (originally looked at desert Hans Fugal <hans@fugal.net> http://hans.fugal.net/vim/colors/desert.vim (2003/05/06)
" Modified By: Simao Gomes Viana

:set background=dark
:highlight clear
if version > 580
 hi clear
 if exists("syntax_on")
 syntax reset
 endif
endif
let colors_name = "cool"
:hi Normal guifg=White guibg=#121212
:hi Cursor guibg=#121212 guifg=slategrey
:hi VertSplit guibg=#c2bfa5 guifg=grey40 gui=none cterm=reverse
:hi Folded guibg=black guifg=grey40 ctermfg=grey ctermbg=darkgrey
:hi FoldColumn guibg=black guifg=grey20 ctermfg=4 ctermbg=7
:hi IncSearch guifg=green guibg=black cterm=none ctermfg=yellow ctermbg=green
:hi ModeMsg guifg=goldenrod guibg=#1 gui=bold cterm=none ctermfg=brown
:hi MoreMsg guifg=SeaGreen ctermfg=darkgreen
:hi NonText guifg=#2c3236 guibg=#161616 cterm=bold ctermfg=grey
:hi Question guifg=springgreen ctermfg=green
:hi Search guibg=#8A3F1A guifg=wheat cterm=none ctermfg=grey ctermbg=blue
:hi SpecialKey guifg=yellowgreen ctermfg=darkgreen
:hi StatusLine guibg=#212121 guifg=white gui=none cterm=bold
:hi StatusLineNC guibg=#c2bfa5 guifg=grey40 gui=none cterm=reverse
:hi Title guifg=gold gui=bold cterm=bold ctermfg=yellow
:hi Statement guifg=#2196F3 ctermfg=lightblue
:hi Visual guibg=#323238 cterm=reverse
:hi WarningMsg guifg=salmon ctermfg=1
:hi String guifg=#1AED7E ctermfg=darkcyan
:hi Comment term=bold ctermfg=11 guifg=grey52
:hi Constant guifg=#F06292 ctermfg=brown
:hi Special guifg=darkkhaki ctermfg=brown
:hi Identifier guifg=#FAFAFA ctermfg=red
:hi Include guifg=#2196F3 gui=bold term=bold ctermfg=red
:hi PreProc guifg=#448AFF gui=bold term=bold ctermfg=red
:hi Operator guifg=#B2DFDB ctermfg=white
:hi Define guifg=#2979FF gui=bold ctermfg=blue
:hi Type guifg=CornflowerBlue ctermfg=2
:hi Function guifg=navajowhite ctermfg=brown
:hi Structure guifg=#18E885 ctermfg=green
:hi LineNr guifg=grey32 ctermfg=3 guibg=#161616
:hi Ignore guifg=grey40 cterm=bold ctermfg=7
:hi Todo guifg=orangered guibg=#282602
:hi Directory ctermfg=darkcyan
:hi ErrorMsg cterm=bold guifg=red guibg=transparent cterm=bold ctermfg=7 ctermbg=1
:hi VisualNOS cterm=bold,underline
:hi WildMenu ctermfg=0 ctermbg=3
:hi DiffAdd ctermbg=4
:hi DiffChange ctermbg=5
:hi DiffDelete cterm=bold ctermfg=4 ctermbg=6
:hi DiffText cterm=bold ctermbg=1
:hi Underlined cterm=underline ctermfg=5
:hi Error guifg=White guibg=Red cterm=bold ctermfg=7 ctermbg=1
:hi SpellErrors guifg=White guibg=Red cterm=bold ctermfg=7 ctermbg=1

" Custom colors

:hi InfoMsg guifg=#1AED7E gui=bold term=bold ctermfg=darkcyan
:hi ExtraMsg guifg=goldenrod guibg=#1 gui=bold cterm=none ctermfg=brown

