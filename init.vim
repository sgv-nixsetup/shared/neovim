call plug#begin(stdpath('data') . '/plugged')

Plug 'lervag/vimtex'

" doesn't work
"Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"let g:deoplete#enable_at_startup = 1

Plug 'Shougo/neosnippet.vim'
Plug 'Shougo/neosnippet-snippets'

" doesn't work
"Plug 'zchee/nvim-go', { 'do': 'make'}

call plug#end()

execute pathogen#infect()

syntax on
filetype plugin indent on

" True Colors
set termguicolors

colorscheme cool

" Mouse integration
set mouse=a
" Line numbers
set number
" Modelines
set modeline

let g:has_started_tex_compilation = 0

let g:vimtex_compiler_latexmk = {
    \ 'build_dir' : 'out',
    \ 'options' : [
    \   '-pdf',
    \   '-shell-escape',
    \   '-verbose',
    \   '-file-line-error',
    \   '-synctex=1',
    \   '-interaction=nonstopmode',
    \   '-output-directory=out',
    \ ],
    \}

function StartTexCompilation()
	if g:has_started_tex_compilation == 0
		echohl InfoMsg
		echomsg "Starting compilation..."
		echohl Normal
		redraw
		normal \ll
		let g:has_started_tex_compilation = 1
	else
		echohl InfoMsg
		echomsg "New buffer, not starting compilation"
		echohl Normal
	endif
endfunction

function StopTexCompilation()
	let l:openbuffers = len(getbufinfo({'buflisted':1})) 
	if openbuffers <= 1
		echohl InfoMsg
		echomsg "Stopping compilation..."
		echohl Normal
		redraw
		normal \ll
		execute "!pkill evince"
		redraw
	else
		echohl InfoMsg
		echomsg openbuffers . " open buffers detected, not stopping compilation"
		echohl Normal
	endif
endfunction

autocmd BufWritePost *.go silent! execute "!goimports -w <afile>" | execute "!go fmt <afile>" | redraw! | edit!

autocmd BufWinEnter *.tex call StartTexCompilation()
autocmd BufWinLeave *.tex call StopTexCompilation()
autocmd BufWinEnter *.de.tex setlocal spell spelllang=de
autocmd BufWinEnter *.en.tex setlocal spell spelllang=en_us
autocmd BufWinEnter *.gb.tex call setlocal spell spelllang=en

let g:livepreview_previewer = 'evince'

" Hide search results
noremap <silent> <F2> :noh<cr>

" Some preferences
set tabstop=4
set shiftwidth=4
set softtabstop=4

